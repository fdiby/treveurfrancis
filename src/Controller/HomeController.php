<?php

namespace App\Controller;




use App\Entity\Peintures;
use App\Repository\PeinturesRepository;
use App\Repository\TemoignagesRepository;
use App\Repository\ArticlesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }
    /**
     * @Route("/", name="home")
     */
    public function index(TemoignagesRepository $repoTemoignages, PeinturesRepository $repoPeintures, ArticlesRepository $repoArticles)
    {
        $peint = $repoPeintures->findBy(['isBest' => true]);
        $temoign = $repoTemoignages->findAll();
        $derniers = $repoArticles->myfindDerniersArticle(2);


        return $this->render('home/index.html.twig', compact('temoign', 'peint', 'derniers'));
    }

    /**
     * @Route("/peinture/{slug}", name="peinture_show")
     */
    public function show($slug)
    {
        $peinture = $this->manager->getRepository(Peintures::class)->findOneBySlug($slug);
        return $this->render("home/show.html.twig", [
            "peinture" => $peinture
        ]);
    }

    /**
     * @Route("/portfolio", name="portfolio")
     */
    public function portfolio(PeinturesRepository $repoPeintures)
    {
        $peintures = $repoPeintures->findAll();
        return $this->render('home/portfolio.html.twig', [
            'peinture' =>  $peintures,
        ]);
    }


    /**
     * @Route("/a_propos", name="a_propos")
     */
    public function apropos(): Response
    {

        return $this->render('home/apropos.html.twig');
    }


    /**
     * @Route("/boutique", name="boutique")
     */
    public function boutique(PeinturesRepository $repoPeintures): Response
    {

        $prix = 0;
        $boutiques = $repoPeintures->findAllPrice($prix);

        return $this->render('home/boutique.html.twig', compact('boutiques'));
    }


    /**
     * @Route("/conditions-générale", name="cgu_conditions")
     */
    public function cgu(): Response
    {
        return $this->render('home/cgu.html.twig');
    }
}
