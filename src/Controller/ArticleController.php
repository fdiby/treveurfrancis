<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Repository\ArticlesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }
    /**
     * @Route("/evenements", name="events")
     */
    public function index(ArticlesRepository $repoArticle)
    {

        $articles = $repoArticle->findAll();
        return $this->render('article/index.html.twig', [

            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/evenements/{slug}", name="events_show")
     */
    public function show($slug)
    {
        $article = $this->manager->getRepository(Articles::class)->findOneBySlug($slug);

        if (!$article) {
            throw $this->createNotFoundException('Pas d\'annonce trouvée');
        }

        return $this->render("article/show.html.twig", [
            'article' => $article
        ]);
    }
}
