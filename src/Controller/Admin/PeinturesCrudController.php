<?php

namespace App\Controller\Admin;

use App\Entity\Peintures;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class PeinturesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Peintures::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [

            TextField::new('titre', 'Un titre'),
            SlugField::new('slug', 'slug')->setTargetFieldName('titre'),
            TextField::new('intro', ' intro'),
            TextField::new('imageFile')->setFormType(VichImageType::class)->onlyWhenCreating(),
            ImageField::new('image')
                ->setLabel('Image')
                ->setBasePath('uploads/peintures')
                ->setUploadDir('public/uploads/peintures')
                ->setUploadedFileNamePattern('[randomhash].[extension]')
                ->setRequired(false),


            AssociationField::new('categories', 'Categories'),

            TextEditorField::new('content', 'Descriptif'),
            BooleanField::new('envente'),
            BooleanField::new('active'),
            BooleanField::new('isBest', ' meilleurs produits?'),
            NumberField::new('prix'),
            DateTimeField::new('createdAt', 'Crée le')

        ];
    }
}
