<?php

namespace App\Controller\Admin;


use App\Entity\User;
use App\Entity\Articles;
use App\Entity\Peintures;
use App\Entity\Category;
use App\Entity\Temoignages;
use App\Repository\CategoryRepository;
use App\Repository\PeinturesRepository;
use App\Repository\ArticlesRepository;
use App\Repository\TemoignagesRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var PeinturesRepository
     */
    protected $peinturesRepository;

    /**
     * @var ArticlesRepository
     */
    protected  $articlesRepository;

    /**
     * @var TemoignagesRepository
     */
    protected  $temoignagesRepository;


    public function __construct(CategoryRepository $categoryRepository, PeinturesRepository $peinturesRepository, ArticlesRepository $articlesRepository, TemoignagesRepository $temoignagesRepository)
    {
        $this->CategoryRepository = $categoryRepository;
        $this->PeinturesRepository = $peinturesRepository;
        $this->ArticlesRepository = $articlesRepository;
        $this->TemoignagesRepository = $temoignagesRepository;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('bundles/EasyAdminBundle/welcome.html.twig', [
            'countCat' => $this->CategoryRepository->countAllCategory(),
            'countPeint' => $this->PeinturesRepository->countAllPeintures(),
            'countArt' => $this->ArticlesRepository->countAllArticles(),
            'countTemoign' => $this->TemoignagesRepository->countAllTemoignages()

        ]);
    }


    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Treveurfrancis');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Tableau de Bord', 'fa fa-home');
        yield MenuItem::linktoCrud('Categories', 'fa fa-list', Category::class);
        yield MenuItem::linktoCrud('Peintures', 'fa fa-tag', Peintures::class);
        yield MenuItem::linktoCrud('Articles', 'fa fa-newspaper ', Articles::class);
        yield MenuItem::linktoCrud('Temoignages', 'fa fa-handshake-o ', Temoignages::class);
        yield MenuItem::linktoCrud('Utilisateurs', 'fa fa-user', User::class);
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
