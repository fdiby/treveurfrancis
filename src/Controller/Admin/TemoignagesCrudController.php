<?php

namespace App\Controller\Admin;

use App\Entity\Temoignages;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class TemoignagesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Temoignages::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [


            TextEditorField::new('contenu', 'Le Temoignage'),
            TextField::new('imageFile')->setFormType(VichImageType::class)->onlyWhenCreating(),
            ImageField::new('illustration')
                ->setLabel('Photo')
                ->setBasePath('uploads/temoignages')
                ->setUploadDir('public/uploads/temoignages')
                ->setUploadedFileNamePattern('[randomhash].[extension]')
                ->setRequired(false),

            TextField::new('auteur', 'Nom auteur'),
            TextField::new('profession', 'Sa profession')
        ];
    }
}
